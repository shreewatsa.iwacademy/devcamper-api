// @desc      Get all bootcamps
// @route     GET /api/v1/bootcamps
// @access    Public (or private)
exports.getBootcamps = (req, res, next) => {
  // res.send("<h1>Hello from express !!</h1>");
  // res.send({ name: "Shreewatsa Timalsena" });
  // res.json({ name: "Shreewatsa Timalsena" });
  // res.sendStatus(400);
  // res.status(400).json({ success: false });
  res
    .status(200)
    .json({ success: true, msg: "Show all bootcamps", hello: req.hello });
};

// @desc      Retrieve single bootcamp
// @route     GET /api/v1/bootcamps/:id
// @access    Public (or private)
exports.getBootcamp = (req, res, next) => {
  res.status(200).json({
    success: true,
    data: { msg: `Retrieve a bootcamp ${req.params.id}.` },
  });
};

// @desc      Create new bootcamp
// @route     POST /api/v1/bootcamps
// @access    Public (or private)
exports.createBootcamp = (req, res, next) => {
  res.status(200).json({ success: true, msg: "Create a new bootcamp" });
};

// @desc      Update new bootcamp
// @route     POST /api/v1/bootcamps/:id
// @access    Private
exports.updateBootcamp = (req, res, next) => {
  res
    .status(200)
    .json({ success: true, msg: `Update a bootcamp ${req.params.id}.` });
};

// @desc      Delete new bootcamp
// @route     POST /api/v1/bootcamps/:id
// @access    Private
exports.deleteBootcamp = (req, res, next) => {
  res.status(200).json({ success: true, msg: "Delete a new bootcamp" });
};
