const express = require("express");
const dotenv = require("dotenv");
const connectDB = require("./config/db");
const colors = require("colors");

const morgan = require("morgan");
const logger = require("./middleware/logger");

// Load env vars
dotenv.config({ path: "./config/config.env" });

// Connect to Database
connectDB();
// Route files
const bootcamps = require("./routes/bootcamps");

const app = express();

// Middleware example
// Dev logging middleware
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}
// app.use(logger);   // our custom middleware for logging.
// morgan is a popular package for logging requests.

//Mount routers
app.use("/api/v1/bootcamps", bootcamps);

const PORT = process.env.PORT || 5000;
const server = app.listen(
  PORT,
  console.log(
    `Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold
  )
);

// Handling the unhandled rejections
process.on("unhandledRejection", (err, promise) => {
  console.log(`Error: ${err.message}.red`);

  //close server and exit process
  server.close(() => process.exit(1));
});
